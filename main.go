package main

import (
	"bufio"
	"fmt"
	"math"
	"os"
	"sort"
	"strconv"
	"strings"
)

// ============================================
func sayGreeting(n string) {
	fmt.Printf("39. Good morning %v \n", n)
}
func sayBye(n string) {
	fmt.Printf("40. Goodbye %v \n", n)
}
func cycleNames(n []string, f func(string)) {
	for _, v:= range n {
		f(v)
	}
}
func circleArea(r float64) float64{
	return math.Pi * r * r
}
// ============================================

func getInitials(n string) (string, string) {
	s := strings.ToUpper(n)
	names__ :=strings.Split(s, " ")

	var initials []string
	for _, v:= range names__ {
		initials = append(initials, v[:1])
	}

	if len(initials) > 1 {
		return initials[0], initials[1]
	}

	return initials[0], "_"
	
}

var score = 99.5

// ============================================
// CHAPTER 18 : USER INPUT (READER)

// helper function so that we can call the function one time to get some user input
func getInput(prompt string, r *bufio.Reader) (string, error) {
	fmt.Print(prompt)
	input, err := r.ReadString('\n')

	return strings.TrimSpace(input), err
}

func createBill() bill {
	reader := bufio.NewReader(os.Stdin)

	/* fmt.Print("54. Create a new bill name: ")
	name, _ := reader.ReadString('\n') // must be in single quotes, it is not a string
	name = strings.TrimSpace(name) */
	name, _ := getInput("54. Part 18: Create a new bill name: ", reader)

	b := newBill(name)
	fmt.Println("54. Created the bill -", b.name)
	
	return b
}
// give user 3 options: a to add an item, s to save the bill, or t to add a tip
func promptOptions(b bill) {
	reader := bufio.NewReader(os.Stdin)
	opt, _ := getInput("55. Part 18: Choose option :\n a - add item,\n s - save bill,\n t - add tip \n which one? ", reader)

	switch opt {
	case "a":
		name, _ := getInput("Item name: ", reader)
		price, _ := getInput("Item price: ", reader)

		p, err := strconv.ParseFloat(price, 64)
		if err != nil {
			fmt.Println("The price must be a number")
			promptOptions(b)
		}
		b.addItem(name, p)

		fmt.Println("item added - ", name, price)
		promptOptions(b)
	case "t":
		tip, _ := getInput("Enter tip amount ($):", reader)

		t, err := strconv.ParseFloat(tip, 64)
		if err != nil {
			fmt.Println("The tip must be a number")
			promptOptions(b)
		}
		b.updateTip(t)
		
		fmt.Println("Tip added - ", tip)
		promptOptions(b)
	case "s":
		b.save() 
		fmt.Println("You saved the file", b.name)
	default:
		fmt.Println("That was not a valid option...")
		promptOptions(b)
	}
	
	fmt.Println(opt)

}

// ============================================

// CHAPTER 22: INTERFACES

// shape interface
type shape interface {
	area() float64
	circumf() float64
}

type square struct {
	length float64
}
type circle struct {
	radius float64
}

// square methods
func (s square) area() float64 {
	return s.length * s.length
}
func (s square) circumf() float64 {
	return s.length * 4
}

// circle methods
func (c circle) area() float64 {
	return math.Pi * c.radius * c.radius
}
func (c circle) circumf() float64 {
	return 2 * math.Pi * c.radius
}

func printShapeInfo(s shape) {
	fmt.Printf("area of %T is: %0.2f \n", s, s.area())
	fmt.Printf("circumference of %T is: %0.2f \n", s, s.circumf())
}

// ============================================

func main() {
	// string
	var nameOne string = "Choii"
	var nameTwo = "Ilung"
	var nameThree string
	fmt.Println("1.", "Hello, 世界 ", nameOne, ",", nameTwo, nameThree)	
	nameOne = "Temi"
	nameThree = "Go"
	fmt.Println("2.", nameOne, nameTwo, nameThree)

	nameFour := "revina"

	fmt.Println("3.", nameOne, nameFour)

	// ints
	var ageOne int = 20
	var ageTwo = 25
	ageThree := 23
	fmt.Println("4.", ageOne, ageTwo, ageThree)

	// bits & memory
	// var numOne int8 = 24
	// var numTwo int8 = -128
	// var numThree uint8 = 255
	fmt.Print("5.","Hello, ")
	fmt.Print("5.", "World! \n")
	fmt.Println("6.", "new line")

	name := "Ahmanet"
	// Printf (formatted strings) %_ = format specifier
	fmt.Printf("7. my age is %v and my name is %v \n", ageThree, name)
	fmt.Printf("8. my age is %q and my name is %q \n", ageOne, name)
	fmt.Printf("9. age is of type %T \n", ageThree)
	fmt.Printf("10. you scored %f points! \n", 225.55)
	fmt.Printf("11. you scored %.1f points! \n", 225.55)
	fmt.Printf("12. you scored %1.1f points! \n", 225.5769)
	
	// Sprintf( save formatted strings )
	var str = fmt.Sprintf("13. \"my age is %v and my name is %v\" ", ageThree, name)
	
	fmt.Println("14.", "the saved string is:", str)
	

	// CHAPTER 5: WORKING WITH ARRAYS
	// var ages [3]int = [3]int{20, 25, 40}
	var ages = [3]int{20, 25, 40}

	names := [4]string{"yoshi", "mario", "peach", "bowser"}
	names[1] = "luigi"

	fmt.Println("15.", ages, len(ages))
	fmt.Println("16.", names, len(names))

	// slices (use arrays under the hood)
	var scores = []int{100, 50, 60}
	scores[2] = 25
	scores = append(scores, 86)
	fmt.Println("17.", scores, len(scores))

	// slice ranges
	rangeOne := names[1:4]
	rangeTwo := names[2:]
	rangeThree := names[:3]
	fmt.Println("18.", rangeOne, rangeTwo, rangeThree)
	
	rangeOne = append(rangeOne, "koopa")
	fmt.Println("19.", rangeOne)


	// CHAPTER 6: THE STANDARD LIBRARY 
	// "Strings" package
	greeting:= "hello there friends!"

	fmt.Println("20.", strings.Contains(greeting, "nds!"))
	fmt.Println("21.", strings.ReplaceAll(greeting, "hello", "konnichiwa"))
	fmt.Println("22.", strings.ToUpper(greeting))
	fmt.Println("23.", strings.Index(greeting, "ere "))
	fmt.Println("24.", strings.Split(greeting, " "))
	// the original value is unchanged
	fmt.Println("25.", "original string value =", greeting)
	
	// "Sort" package
	umur := []int{45, 20, 35, 30, 75, 60, 50, 25}
	sort.Ints(umur)
	fmt.Println("26.", umur)

	index:=sort.SearchInts(umur, 30)
	fmt.Println("27.", index)

	names_ := []string{"yoshi", "mario", "peach", "bowser", "luigi"}
	sort.Strings(names_)
	fmt.Println("28.", names_)
	fmt.Println("29.", sort.SearchStrings(names_, "luigi"))


	// CHAPTER 7: LOOPS	
	// while loop
	x:=0
	for x < 5 {
		fmt.Println("30.", "value of x is:", x)
		x++
	}

	// for loop
	for i := 0; i < 5; i++ {
		fmt.Println("31.", "value of i is:", i)
	}
	for i := 0; i < len(names_); i++ {
		fmt.Println("32.", names_[i])
	}
	for index, value := range names_ {
		fmt.Printf("33. the value at index %v is: %v \n", index, value)
	}
	for _, value := range names_ {
		fmt.Printf("33. the value is: %v \n", value)
	}


	// CHAPTER 8: BOOLEANS AND CONDITIONS
	age:=45
	fmt.Println("34.", age <= 50, age >= 50, age == 45, age!=50)

	if age < 30 {
		fmt.Println("35. age is less than 30")
	} else if age < 40 {
		fmt.Println("35. age is less than 40")
	} else {
		fmt.Println("35. age is not less than 45")
	}

	// using names_ variable to the nested conditional
	for index_, value_ := range names_ {
		if index_ == 1 {
			fmt.Println("36. continuing at pos", index_)
			continue
		}
		if index_ > 2 {
			fmt.Println("38. breaking at pos", index_)
			break
		}
		fmt.Printf("37. the value at pos %v is %v \n", index_, value_)
	}


	// CHAPTER 9: USING FUNCTIONS
	// sayGreeting("Mario")
	// sayGreeting("Luigi")
	// sayGreeting("Bowser")


	cycleNames([]string{"cloud", "tifa", "barret"}, sayGreeting)
	cycleNames([]string{"cloud", "tifa", "barret"}, sayBye)

	a1 := circleArea(10.5)
	a2 := circleArea(15)

	fmt.Printf("41. circle 1 is %0.3f and circle 2 is %0.2f \n", a1, a2)


	// CHAPTER 10: FUNCTION WITH MULTIPLE RETURN VALUES
	firstName1, secondName1 := getInitials("Teman lama")
	fmt.Println("42. ", firstName1, secondName1)

	firstName2, secondName2 := getInitials("Benda Terbang Tak Dikenal")
	fmt.Println("43. ", firstName2, secondName2)

	firstName3, secondName3 := getInitials("Cinta Lama Bersemi Kembali")
	fmt.Println("44. ", firstName3, secondName3)

	firstName4, secondName4 := getInitials("Momod")
	fmt.Println("45. ", firstName4, secondName4)


	// CHAPTER 11: PACKAGE SCOPE
	sayHello("mario")

	for _, v := range points {
		fmt.Println("47. This is from greeting.go where there is a slices variable named \"points\": ", v)

	}

	showScore()


	// CHAPTER 12: MAPS 
	menu:= map[string]float64{
		"soup": 4.99,
		"pie": 8.99,
		"salad": 6.99,
		"toffee pudding": 3.55,
	}
	fmt.Println("49. ", menu)
	fmt.Println("50.", menu["pie"])
	// looping maps 
	for k, v := range menu {
		fmt.Println("51. Mapping the menu:", k, "-", v)
	}

	// ints as key type
	phonebook := map[int]string {
		26791648 : "mario",
		26791249 : "luigi",
		26793059 : "peach",
	}

	fmt.Println("52.", phonebook)
	fmt.Println("52.", phonebook[26791648])
	

	// CHAPTER 15: STRUCTS & CUSTOM TYPES
	billSaya := newBill("mario's bill")

	billSaya.addItem("pancake", 4.49)
	billSaya.addItem("veg pie", 8.95)
	billSaya.addItem("toffee pudding", 4.95)
	billSaya.addItem("coffee", 3.25)
	billSaya.updateTip(10)
		
	fmt.Println("53.", billSaya.format())
	

	// CHAPTER 18: USER INPUT (READER)
	myBill := createBill()
	promptOptions(myBill)

	fmt.Println("54.", myBill)


	// CHAPTER 22: INTERFACES
	shapes := []shape{
		square{length: 15.2},
		circle{radius: 7.5},
		circle{radius: 12.3},
		square{length: 4.9},
	}

	for _, v := range shapes {
		printShapeInfo(v)
		fmt.Println("---")
	}
	
	
}

