// CHAPTER 14
package main

import (
	"fmt"
)

func updateName(x *string) {
	*x= "wedge"
}

func main() {
	name:="tifa"
	// updateName(name)
	// fmt.Println("memory address of name is: ", &name)
	m := &name
	fmt.Println("memory address:", m)
	fmt.Println("value at memory address:", *m)
	

	fmt.Println(name)
	updateName(m)
	fmt.Println(name)
	
}

/* 
|---name---|----m---|
|  0x001   | 0x002  |
|----------|--------|
|  "tifa"  | p0x001 |
|----------|--------|
*/
// the name in line 10 is stored in 0x001 memory block, and we also have m (line 13) is stored in 0x002 memory block (its own memory block) but it is storing the pointer to the name memory block (0x001) where tifa is stored