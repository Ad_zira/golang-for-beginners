package main

import (
	"fmt"
	"os"
)

type bill struct {
	name string
	items map[string]float64
	tip float64
}

// import "fmt"
// make new bills CHAPTER 16: RECEIVER FUNCTIONS
// func newBill(name string) bill {
// 	b := bill{
// 		name: name,
// 		items: map[string]float64{"pie": 5.99, "cake": 3.99, "fruit-dessert": 4.49, "black-forest gateau": 59.99},
// 		tip: 0,
// 	}

// 	return b
// }

// CHAPTER 17: RECEIVER FUNCTIONS WITH POINTERS 
func newBill(name string) bill {
	b := bill{
		name: name,
		items: map[string]float64{},
		tip: 0,
	}

	return b
}


// format the bill (The receiver functions goes by "(b bill)", where we receive the bill object)
// adding * (pointer) to the bill struct 
func (b *bill) format() string {
	formattedStr:="Bill breakdown:  \n"
	var total float64 = 0

	// list items
	for k, v := range b.items {
		formattedStr += fmt.Sprintf("%-25v: ...$%v \n", k, v)
		total += v
	}

	// add tip
	formattedStr += fmt.Sprintf("%-25v: ...$%v\n", "tip", b.tip)
	
	// total 
	formattedStr += fmt.Sprintf("%-25v: ...$%0.2f", "total", total+b.tip)
	return formattedStr
}

// the updateTip and addItem functions were also being used in CHAPTER 17 (TUTORIAL 17) 
// update tip CHAPTER 16
// func(b bill) updateTip(tip float64) {
// 	b.tip = tip
// }

// update tip CHAPTER 17; use asterisk to "point" the bill struct 
func(b *bill) updateTip(tip float64) {
	b.tip = tip
}

// add an item to the bill
func (b *bill) addItem(name string, price float64) {
	b.items[name] = price;
}

// CHAPTER 21: SAVING FILES
// save bill
func (b *bill) save() {
	data := []byte(b.format()) // we want to turn our formatted string of the bill into a byte slice. To do that we can just use parentheses and pass in b.format(). So we format the bill we get a string back and we turn that into a byte slice and we re storing it inside the datavariable

	err := os.WriteFile("bills/"+b.name+".txt", data, 0644)
	if err != nil {
		panic(err)
	}
	fmt.Println("bill was saved to file")
}


